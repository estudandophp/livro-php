<?php

  echo getWeekDay().", ".date('d')." de ".getNameMonth()." de ".date('Y');
  echo "<br>";
  echo date('H:i:s');
  echo "<br>";
  echo "Faltam ".untilSaturday()." dias para sábado!";

  function getNameMonth(){

    switch (date('n')) {
      case '1':
        return "Janeiro";
        break;

      case '2':
        return "Fevereiro";
        break;

      case '3':
        return "Março";
        break;

      case '4':
        return "Abril";
        break;

      case '5':
        return "Maio";
        break;

      case '6':
        return "Junho";
        break;

      case '7':
        return "Julho";
        break;

      case '8':
        return "Agosto";
        break;

      case '9':
        return "Setembro";
        break;

      case '10':
        return "Outubro";
        break;

      case '11':
        return "Novembro";
        break;

      case '12':
        return "Dezembro";
        break;

      default:
        return "Erro: Mês inexistente";
        break;
    }

  }

  function untilSaturday(){
    return 6 - date('w');
  }

  function getWeekDay(){

    switch (date('w')) {
      case '0':
        return "Domingo";
        break;

      case '1':
        return "Segunda";
        break;

      case '2':
        return "Terça";
        break;

      case '3':
        return "Quarta";
        break;

      case '4':
        return "Quinta";
        break;

      case '5':
        return "Sexta";
        break;

      case '6':
        return "Sábado";
        break;

      default:
        return "Erro: dia da semana inexistente";
        break;
    }

  }
?>
